ProjectEuler
============

Project Euler solutions in Python 2 www.projecteuler.net/ \

### Progress Log

#### Problem 1

-   Done! 1/21/2013
-   [projecteuler.net/problem=1](http://projecteuler.net/problem=1)
-   Implemented list comprehension in sum() to find the sum of the
    multiples of 3 and 5 up to 1000

#### Problem 2

-   Done! 1/21/2013
-   [projecteuler.net/problem=2](http://projecteuler.net/problem=2)
-   Implemented common Fibonacci series generator

#### Problem 3

-   Done! 1/22/2013
-   [projecteuler.net/problem=3](http://projecteuler.net/problem=3)
-   Made one function to check if an int is prime
-   Another function to find prime factors of a given int
-   OverflowError solved by using the square root of the given int as
    the end of the range

#### Problem 4

-   Done! 7/13/2013
-   [projecteuler.net/problem=4](http://projecteuler.net/problem=4)
-   Used integers as strings to determine if the product was a
    palindrome.
-   Decided to make the program scalable also by using integeres as
    strings (concatenated char zeroes)

#### Problem 5

-   Done! 7/13/2013
-   [projecteuler.net/problem=5](http://projecteuler.net/problem=5)
-   Took optimization advice from
    [StackOverflow](http://stackoverflow.com/questions/8024911/project-euler-5-in-python-how-can-i-optimize-my-solution)

#### Problem 6

-   Done! 7/13/2013
-   [projecteuler.net/problem=6](http://projecteuler.net/problem=6)
-   Too easy.

#### Problem 7

-   Done! 7/13/2013
-   [projecteuler.net/problem=7](http://projecteuler.net/problem=7)
-   Simple stuff. Used the same function from problem 3
-   Added a counter and a while loop to find the nth prime number

#### Problem 8

-   Done! 7/13/2013
-   [projecteuler.net/problem=8](http://projecteuler.net/problem=8)
-   Converted thousand digit number into string in order to create a
    list of its digits
-   Then simply made another list of sublists of five consecutive
    numbers incrementally
-   Used operator module tip from
    [StackOverflow](http://stackoverflow.com/questions/2104782/returning-the-product-of-a-list)
    to return a list of the products of each sublist

#### Problem 9

- Done! 7/13/2013

- [projecteuler.net/problem=9](http://projecteuler.net/problem=9)

- Tried multiples of 3, 4, and 5; unsuccessful

- Resorted to Pythagorean Triples construct:

-   Where n and m are any two positive integers (m \< n):
-   [![](http://latex.codecogs.com/gif.latex?a&space;=&space;n^{2}&space;-&space;m^{2} "a = n^{2} - m^{2}")](http://www.codecogs.com/eqnedit.php?latex=a&space;=&space;n^{2}&space;-&space;m^{2})
-   [![](http://latex.codecogs.com/gif.latex?b&space;=&space;2nm "b = 2nm")](http://www.codecogs.com/eqnedit.php?latex=b&space;=&space;2nm)
-   [![](http://latex.codecogs.com/gif.latex?c&space;=&space;n^{2}&space;+&space;m^{2} "c = n^{2} + m^{2}")](http://www.codecogs.com/eqnedit.php?latex=c&space;=&space;n^{2}&space;+&space;m^{2})

#### Problem 10

-   Done! 7/13/2013
-   [projecteuler.net/problem=10](http://projecteuler.net/problem=10)
-   Used same fuctions from problem 3 and 7
-   In is\_prime(), I made the end of the range the square root of the
    given int to drastically speed up the program
-   Earned 'Decathlete' award: completed 10 consecutive problems

#### Problem 11

-   Done! 7/14/2013
-   [projecteuler.net/problem=11](http://projecteuler.net/problem=11)
-   Turned in string into grid of integers
-   Grouped factors into sublists of horizontal, vertical, diagonal left
    and right
-   Changed range limits to fix index issues

#### Problem 12

-   Done! 7/16/2013
-   [projecteuler.net/problem=12](http://projecteuler.net/problem=12)
-   Need to optimize
-   get\_number\_of\_factors() is problematic
-   Fixed! 7/16/2013
-   Optimized by using xrange() instead of range() to reduce resource
    usage
-   get\_number\_of\_factors() refactored with advice from
    [codereview.stackexchange.com](http://codereview.stackexchange.com/questions/28527/simple-code-that-finds-number-of-factors-crashes-my-computer-is-there-a-workaro/28533?noredirect=1#28533)

#### Problem 13

-   Done! 7/19/2013
-   [projecteuler.net/problem=13](http://projecteuler.net/problem=13)
-   Took a while to figure out what the problem was asking
-   Fairly straight forward after figured that out
-   Used list comprehension to get sum of the 50-digit numbers and
    printed the first ten digits

#### Problem 14

-   Done! 7/20/2013
-   [projecteuler.net/problem=14](http://projecteuler.net/problem=14)
-   Used a for loop to iterate the starting numbers and a while loop to
    find/count the chain
-   Had an 'Off-by-one' error when counting the chain

#### Problem 15

- Done! 7/21/2013

- [projecteuler.net/problem=15](http://projecteuler.net/problem=15)

- Used a horrific brute force method at first

- Problem most likely lies in the random generator of the next move

- With many, many trials and lots of brainstorming, I concluded that the
    total number of moves is equal to twice the size of the grid (20 Right,
    20 Down)

- Left script running overnight and still no result

- After a bit of research in combinations and permutations, I found a
    formula for the total number of non-repeating combinations:

-   Binomial Coefficient (where n is the number of things to choose
    from, and you choose r of them and repetition and order don't
    matter)
-   ![](http://www.mathsisfun.com/combinatorics/images/combinations-no-repeat.png)
-   [Combinations and
    Permutations](http://www.mathsisfun.com/combinatorics/combinations-permutations.html)

Implentation of formula and refactoring produced immensely faster
results:

-   Brute force method: inconclusive (runtime \> 10 hours)
-   Binomial Coefficient: 0.002 seconds

#### Problem 16

-   Done! 7/17/2013
-   [projecteuler.net/problem=16](http://projecteuler.net/problem=16)
-   Another one-liner

#### Problem 17

-   Done! 7/17/2013
-   [projecteuler.net/problem=17](http://projecteuler.net/problem=17)
-   Could be done better. Mostly hardcoded.
-   Used a dictionary to store number words
-   Used similar concept from problem 16

#### Problem 18

-   Done! 7/31/2013
-   [projecteuler.net/problem=18](http://projecteuler.net/problem=18)
-   Tried altering the algorithm from problem 17, but was inconclusive
-   Tried a simpler approach. Start from the bottom and add the numbers
    upwards to ensure the highest possible sum
-   Issue with comparing sums: iteration goes out of bounds
-   [Fixed!] Made a seperate function, largest\_sum(), to simplify
    things
-   Got rid of the useless row size counter. Only made it more
    complicated.
-   Used the same concepts in C++ solution

#### Problem 19

-   Done! 8/3/2013
-   [projecteuler.net/problem=19](http://projecteuler.net/problem=19)
-   In C++ [Incomplete], stuck in infinite recursion
-   Counter did not increment in recursion. Used pass-by-reference to
    fix
-   In Python, tried a completely different approach. Still used same
    isLeapYear() function
-   Seems more efficent, however it produces the wrong final answer.
-   Off-by-one error somewhere
-   [Temp fix] Started counter at 1

#### Problem 20

-   Done! 7/18/2013
-   [projecteuler.net/problem=20](http://projecteuler.net/problem=20)
-   Pretty much straight forward
-   Used the factorial formula given in problem description: n! = n \*
    (n - 1)
-   Turned the factorial into a string to get the sum of digits

#### Problem 21

-   Done! 8/2/2013
-   [projecteuler.net/problem=21](http://projecteuler.net/problem=21)
-   Completed Level 1!
-   Used a dictionary to store number[key] and sum of divisors[value]
-   Reduced to list comprehensions
-   Applying similar concepts to C++ solution, but is difficult to
    replace dictionaries
-   Getting float-value error

#### Problem 22

-   Done! 8/4/2013
-   [projecteuler.net/problem=22](http://projecteuler.net/problem=22)
-   Fairly straight forward
-   Used list comprehensions, enumerate(), and, the recently learned,
    replace()

#### Problem 23

-   Incomplete 8/4/2013
-   [projecteuler.net/problem=23](http://projecteuler.net/problem=23)
-   Need to optimize isAbundant(); does not complete in a reasonable
    amount of time
-   After some research, I've learned about generators to get factors of
    an int
-   8/5/2013 When run, outputs "KILLED"

#### Problem 25

-   Done! 7/18/2013
-   [projecteuler.net/problem=25](http://projecteuler.net/problem=25)
-   Used the same fibonacci series generator from problem 2 with slight
    refactorization

#### Problem 26

-   Done! 8/11/2013
-   Took forever to figure out
-   After some research, I found and implemented Fermat's little theorem
-   Off by one error somehow. Kludged it for now

#### Problem 28

-   Done! 8/15/2013
-   [projecteuler.net/problem=28](http://projecteuler.net/problem=28)
-   Made a generator for the spiral
-   The numbers added were only the corners; not including 1
-   1st round skipped 1 number, 2nd round skipped 3, and so forth

#### Problem 29

-   Done! 7/24/2013
-   [projecteuler.net/problem=29](http://projecteuler.net/problem=29)
-   Completed in both Python and C++. Was a lot easier to code in Python
-   Implemented recently learned concept of vectors (still not confident
    with them yet)

#### Problem 30

-   Done! 8/15/2013
-   [projecteuler.net/problem=30](http://projecteuler.net/problem=30)
-   Completed in C++
-   Found upper bound and number of digits by multiplying 9 to the nth
    (i.e. 9\^4 is the max of one digit)

#### Problem 34

-   Done! 10/12/2013
-   [projecteuler.net/problem=34](http://projecteuler.net/problem=34)
-   Completed in Java
-   Fairly straight forward
-   Could have hardcoded the factorials (digits only 0...9) but used a
    simple function instead
-   Classic off-by-one error in factsum()

#### Problem 35

-   Done! 10/13/2013
-   [projecteuler.net/problem=35](http://projecteuler.net/problem=35)
-   Completed in Java
-   Used strings to rotate numbers

#### Problem 42

-   Done! 10/16/2013
-   [projecteuler.net/problem=42](http://projecteuler.net/problem=42)
-   Completed in Java
-   Learned more Java string methods e.g. contains()
-   Created a list of the triangle numbers
-   Calculated word value using a string and letter index values

#### Problem 47

-   Done! 7/24/2013
-   Used a brute force method
-   Fairly easy, however, limiting the range to the sqrt of num doesn't
    print the correct answer
-   Could use a lot of optimization: runtime \> 1500 seconds

#### Problem 48

-   Done! 7/23/2013
-   Another easy one
-   Could have done a one-liner but opted for readability instead

